﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ordinary_Differential_Equations
{
    delegate double Funct(double x, double y);
    class DifEqu
    {
        Funct Function;
        int countStep;
        int accurancy;
        double lenghtStep;
        double a,b;
        
        public DifEqu(Funct function)
        {
            Function = function;
        }



        public void SetRounge(double LeftPoint, double RightPoint)
        {
            if (LeftPoint < RightPoint)
            {
                a = LeftPoint;
                b = RightPoint;
            }
            else
            {
                b = LeftPoint;
                a = RightPoint;
            }
                

        }

        public void SetAccurncy(int Accurancy)
        {
            if (Accurancy == 1 || Accurancy == 2 || Accurancy == 4)
            {
                accurancy = Accurancy;
            }
            else
                accurancy = 1;            
        }        

        public void SetCountStep(int CountStep)
        {
            if (CountStep > 0)
            {
                countStep = CountStep;
            }
        }

        public void SetFunction(Funct function)
        {
            if (function != null)
                Function = function;
        }
            
        private double RoungeKyt4(double Xi, double yi, double h)//вот этот метод рунге кутта
        {
            double k1 = Function(Xi, yi);//дальше идет формула которую можно найти в конспектах и в интернете
            double k2 = Function(Xi + h / 2, yi + h / 2 * k1);
            double k3 = Function(Xi + h / 2, yi + h / 2 * k2);
            double k4 = Function(Xi + h, yi + h+k3);

            return yi + h * (k1 + 2 * k2 + 2 * k3 + k4) / 6;
        }

        delegate double Method(double xi, double yi, double h);

        public double Solve()
        {
            double result=-1;
            Method UseMethod = GetMethod();//здесь присваивается метод которым будет решаться

            double h = (b - a) / countStep;//здесь расчитываеться длина шага
            double xi = a, yi =  0;//Первые корни

            for(int i = 0; i< countStep; i++)//метод рунге-кутта используеться столько раз, сколько мы указали
            {
                xi = a + h * i;//берем следующий x и используем его для для формулы
                yi = UseMethod(xi, yi, h);// Используем метод(рунге кутта в данном случае), находим новый yi

                Console.WriteLine("Промежуточные корни x={0} y={1}", xi, yi);//выводим промежуточные корни
            }

            return yi;//Решение закончено мы нашли рунге кутта

        }

        Method GetMethod()
        {
            Method UseMethod = null;
            if (accurancy == 1)
                UseMethod = SolveByEulerMethod;
            else if (accurancy == 2)
                UseMethod = SolveByMethodSecAccurancy;
            else if (accurancy == 4)
                UseMethod = RoungeKyt4;//здесь метод рунге-кута

            return UseMethod;
        }

        double SolveByEulerMethod(double xi, double yi, double h)
        {
            
            return yi + h * Function(xi, yi);

        }

        double SolveByMethodSecAccurancy(double xi, double yi, double h)
        {
            return  yi + h * Function(xi + h/2, yi+h/2*Function(xi,yi));
        }

        double SolveByMethodSimpson(double xi, double yi, double h)
        {
            double k1 = h * Function(xi, yi);
            double k2 = h * Function(xi + h / 2, yi + k1 / 2);
            double k3 = h * Function(xi + h / 2, yi + k2 / 2);
            double k4 = h * Function(xi + h, yi + k3);
            return yi + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            //Главный метод. Запускает тестовое решение
            RunTest();
            Console.ReadKey();
        }

        public static void RunTest()
        {
            DifEqu _difyr = new DifEqu(TestFunction);//Создаеться класс который берет в себя параметры рысчета и выводит ответ сюда передаеться функция->(TestFunction)
            _difyr.SetRounge(-10, 5);//Это область в которой ищеться решение
            _difyr.SetAccurncy(4);//здесь задаеться каким методом решать
            _difyr.SetCountStep(20);//здесь указываеться количество шагов
            _difyr.Solve();//Этот метод решает и выводит на консоль
            
        }

        //вот эта функция
        static double TestFunction (double x, double y)
        {
            return x * x + y * y;
        }
    }
}
